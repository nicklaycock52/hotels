'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Hotel = mongoose.model('Hotel'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Hotel
 */
exports.create = function (req, res) {
  var hotel = new Hotel(req.body);
  hotel.user = req.user;

  hotel.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(hotel);
    }
  });
};

/**
 * Show the current Hotel
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var hotel = req.hotel ? req.hotel.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  hotel.isCurrentUserOwner = req.user && hotel.user && hotel.user._id.toString() === req.user._id.toString();

  res.jsonp(hotel);
};

/**
 * Update a Hotel
 */
exports.update = function (req, res) {
  var hotel = req.hotel;

  hotel = _.extend(hotel, req.body);

  hotel.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(hotel);
    }
  });
};

/**
 * Delete an Hotel
 */
exports.delete = function (req, res) {
  var hotel = req.hotel;

  hotel.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(hotel);
    }
  });
};

/**
 * List of Hotels
 */
exports.list = function (req, res) {
  var hotels = [
    {
      'id': 'cxd650nuyo',
      'title': 'Courtyard by Marriott Sydney-North Ryde',
      'address': '7-11 Talavera Rd, North Ryde',
      'image': 'https://unsplash.it/145/125/?random',
      'rating': 4,
      'ratingType': 'self',
      'promotion': 'Exclusive Deal',
      'name': 'Deluxe Balcony Room',
      'price': 329,
      'savings': 30,
      'freeCancellation': true
    }, {
      'id': 'mesq6mggyn',
      'title': 'Primus Hotel Sydney',
      'address': '339 Pitt St, Sydney',
      'image': 'https://unsplash.it/145/125/?random',
      'rating': 5,
      'ratingType': 'self',
      'promotion': 'Exclusive Deal',
      'roomName': 'Deluxe King',
      'price': 375,
      'savings': 28,
      'freeCancellation': true
    }, {
      'id': 'xbtlihs45t',
      'title': 'Rydges World Square Sydney',
      'address': '389 Pitt Street, Sydney',
      'image': 'https://unsplash.it/145/125/?random',
      'rating': 4.5,
      'ratingType': 'star',
      'promotion': 'Red Hot',
      'roomName': 'Deluxe King Room',
      'price': 227,
      'savings': 0,
      'freeCancellation': false
    }, {
      'id': '5lm8loqk1s',
      'title': 'PARKROYAL Darling Harbour Sydney',
      'address': '150 Day Street, Sydney',
      'image': 'https://unsplash.it/145/125/?random',
      'rating': 4.5,
      'ratingType': 'star',
      'promotion': 'Red Hot',
      'roomName': 'Darling Harbour Club Room',
      'price': 535,
      'savings': 0,
      'freeCancellation': true
    }, {
      'id': 'kwjf8jlxg9',
      'title': 'Metro Hotel Marlow Sydney Central',
      'address': '431-439 Pitt Street, Sydney',
      'image': 'https://unsplash.it/145/125/?random',
      'rating': 3.5,
      'ratingType': 'star',
      'promotion': 'Bonus Points',
      'roomName': 'Deluxe Triple',
      'price': 295,
      'savings': 0,
      'freeCancellation': true
    }
  ];

  res.jsonp(hotels);
};

/**
 * Hotel middleware
 */
exports.hotelByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Hotel is invalid'
    });
  }

  Hotel.findById(id).populate('user', 'displayName').exec(function (err, hotel) {
    if (err) {
      return next(err);
    } else if (!hotel) {
      return res.status(404).send({
        message: 'No Hotel with that identifier has been found'
      });
    }
    req.hotel = hotel;
    next();
  });
};
