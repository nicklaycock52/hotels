(function () {
  'use strict';

  var component,
    hotels,
    $httpBackend,
    Authentication,
    HotelsService,
    $scope;

  describe('Hotel List compoonent tests', function () {

    beforeEach(function () {
      jasmine.addMatchers({
        toEqualData(util, customEqualityTesters) {
          return {
            compare(actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });

    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($controller, $componentController, _$rootScope_, _Authentication_, _$httpBackend_, _HotelsService_) {
      $scope = _$rootScope_.new;
      Authentication = _Authentication_;
      Authentication.user = { _id: 'test', displayName: 'Test Person' };
      $httpBackend = _$httpBackend_;
      HotelsService = _HotelsService_;
      hotels = [
        {
          'id': 'cxd650nuyo',
          'title': 'Courtyard by Marriott Sydney-North Ryde',
          'address': '7-11 Talavera Rd, North Ryde',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 4,
          'ratingType': 'self',
          'promotion': 'Exclusive Deal',
          'name': 'Deluxe Balcony Room',
          'price': 329,
          'savings': 30,
          'freeCancellation': true
        },
        {
          'id': 'mesq6mggyn',
          'title': 'Primus Hotel Sydney',
          'address': '339 Pitt St, Sydney',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 5,
          'ratingType': 'self',
          'promotion': 'Exclusive Deal',
          'roomName': 'Deluxe King',
          'price': 375,
          'savings': 28,
          'freeCancellation': true
        },
        {
          'id': 'xbtlihs45t',
          'title': 'Rydges World Square Sydney',
          'address': '389 Pitt Street, Sydney',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 4.5,
          'ratingType': 'star',
          'promotion': 'Red Hot',
          'roomName': 'Deluxe King Room',
          'price': 227,
          'savings': 0,
          'freeCancellation': false
        },
        {
          'id': '5lm8loqk1s',
          'title': 'PARKROYAL Darling Harbour Sydney',
          'address': '150 Day Street, Sydney',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 4.5,
          'ratingType': 'star',
          'promotion': 'Red Hot',
          'roomName': 'Darling Harbour Club Room',
          'price': 535,
          'savings': 0,
          'freeCancellation': true
        },
        {
          'id': 'kwjf8jlxg9',
          'title': 'Metro Hotel Marlow Sydney Central',
          'address': '431-439 Pitt Street, Sydney',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 3.5,
          'ratingType': 'star',
          'promotion': 'Bonus Points',
          'roomName': 'Deluxe Triple',
          'price': 295,
          'savings': 0,
          'freeCancellation': true
        }
      ];
      component = $componentController('hotelList', {
        $scope: $scope,
        HotelsService: HotelsService
      }, { });

      // $httpBackend.whenPOST('/api/shifts/confirmedByUserId').respond(200, confirmedShifts);
      // $httpBackend.whenGET('/api/staffattributes').respond(200, existingStaffAttributes);

      $httpBackend.expectGET('/api/hotels').respond(200, hotels);
      $httpBackend.whenGET('/modules/core/client/views/home.client.view.html').respond(200);
      $httpBackend.whenGET('/modules/core/client/views/404.client.view.html').respond(200);
    }));

    describe('Init component', function () {
      it('should load the init function', () => {
        // arrange

        // act
        component.$onInit();
        $httpBackend.flush();

        expect(component.hotels.length).toBe(5);
      });
    });
  });
}());
