(function () {
  'use strict';

  var component,
    component2,
    hotels,
    $httpBackend,
    Authentication,
    HotelsService,
    $scope;

  describe('Hotel List compoonent tests', function () {

    beforeEach(function () {
      jasmine.addMatchers({
        toEqualData(util, customEqualityTesters) {
          return {
            compare(actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });

    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($controller, $componentController, _$rootScope_, _Authentication_, _$httpBackend_, _HotelsService_) {
      $scope = _$rootScope_.new;
      Authentication = _Authentication_;
      Authentication.user = { _id: 'test', displayName: 'Test Person' };
      $httpBackend = _$httpBackend_;
      HotelsService = _HotelsService_;
      hotels = [
        {
          'id': 'cxd650nuyo',
          'title': 'Courtyard by Marriott Sydney-North Ryde',
          'address': '7-11 Talavera Rd, North Ryde',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 4,
          'ratingType': 'self',
          'promotion': 'Exclusive Deal',
          'name': 'Deluxe Balcony Room',
          'price': 329,
          'savings': 30,
          'freeCancellation': true
        },
        {
          'id': 'mesq6mggyn',
          'title': 'Primus Hotel Sydney',
          'address': '339 Pitt St, Sydney',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 5,
          'ratingType': 'self',
          'promotion': 'Exclusive Deal',
          'roomName': 'Deluxe King',
          'price': 375,
          'savings': 28,
          'freeCancellation': true
        },
        {
          'id': 'xbtlihs45t',
          'title': 'Rydges World Square Sydney',
          'address': '389 Pitt Street, Sydney',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 4.5,
          'ratingType': 'star',
          'promotion': 'Red Hot',
          'roomName': 'Deluxe King Room',
          'price': 227,
          'savings': 0,
          'freeCancellation': false
        },
        {
          'id': '5lm8loqk1s',
          'title': 'PARKROYAL Darling Harbour Sydney',
          'address': '150 Day Street, Sydney',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 4.5,
          'ratingType': 'star',
          'promotion': 'Red Hot',
          'roomName': 'Darling Harbour Club Room',
          'price': 535,
          'savings': 0,
          'freeCancellation': true
        },
        {
          'id': 'kwjf8jlxg9',
          'title': 'Metro Hotel Marlow Sydney Central',
          'address': '431-439 Pitt Street, Sydney',
          'image': 'https://unsplash.it/145/125/?random',
          'rating': 3.5,
          'ratingType': 'star',
          'promotion': 'Bonus Points',
          'roomName': 'Deluxe Triple',
          'price': 295,
          'savings': 0,
          'freeCancellation': true
        }
      ];
      component = $componentController('hotel', {
        $scope: $scope,
        HotelsService: HotelsService
      }, { hotel: hotels[0] });

      component2 = $componentController('hotel', {
        $scope: $scope,
        HotelsService: HotelsService
      }, { hotel: hotels[1] });

      $httpBackend.whenGET('/modules/core/client/views/home.client.view.html').respond(200);
      $httpBackend.whenGET('/modules/core/client/views/404.client.view.html').respond(200);
    }));

    describe('Init component', function () {
      it('should load the init function and have binding present', () => {
        // arrange

        // act
        component.$onInit();
        $httpBackend.flush();

        expect(component.hotel.title).toBe('Courtyard by Marriott Sydney-North Ryde');
      });

      it('should be able to get room name', () => {
        // arrange

        // act
        let roomName = component.getRoomName();

        // assert
        expect(roomName).toBe('Deluxe Balcony Room');
      });

      it('should be able to get the room name if the variable is roomTitle', () => {

        // arrange
        // act
        let roomName = component2.getRoomName();

        // assert
        expect(roomName).toBe('Deluxe King');
      });

      it('should check if there is a savings which is true', function () {
        // arrange

        // act
        let savings = component.hasSavings();
        // assert
        expect(savings).toBeTruthy();
      });

      it('should check if there is a savings which is false', function () {
        // arrange
        component.hotel.savings = 0;
        // act
        let savings = component.hasSavings();
        // assert
        expect(savings).not.toBeTruthy();
      });

      it('should return true for star rating', function () {
        // arrange
        component.hotel.ratingType = 'star';
        // act
        let isStarRating = component.isStarRating();
        // assert
        expect(isStarRating).toBeTruthy();
      });

      it('should return circle for rating', function () {
        // arrange

        // act
        let isStarRating = component.isStarRating();
        // assert
        expect(isStarRating).not.toBeTruthy();
      });
    });
  });
}());
