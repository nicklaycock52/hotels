(function () {
  'use strict';

  angular.module('hotels')
    .directive('averageCircleRating', function () {
      return {
        restrict: 'EA',
        template: '<div class=\'average-rating-container\'>' +
          '  <ul class=\'rating background\' class=\'readonly\'>' +
          '    <li ng-repeat=\'circle in circles\' class=\'star\'>' +
          '      <i class=\'fa fa-circle\'></i>' + // &#9733
          '    </li>' +
          '  </ul>' +
          '  <ul class=\'rating foreground\' class=\'readonly\' style=\'width:{{filledInStarsContainerWidth}}%\'>' +
          '    <li ng-repeat=\'circle in circles\' class=\'star filled\'>' +
          '      <i class=\'fa fa-circle\'></i>' + // &#9733
          '    </li>' +
          '  </ul>' +
          '</div>',
        scope: {
          averageRatingValue: '=ngModel',
          max: '=?', // optional: default is 5
          isStar: '='
        },
        link: function (scope, elem, attrs) {
          if (scope.max == undefined) {
            scope.max = 5;
          }

          function updateStars() {
            scope.circles = [];
            for (var i = 0; i < scope.max; i++) {
              scope.circles.push({});
            }
            var starContainerMaxWidth = 100; // %
            scope.filledInStarsContainerWidth = scope.averageRatingValue / scope.max * starContainerMaxWidth;
          }
          scope.$watch('averageRatingValue', function (oldVal, newVal) {
            if (newVal) {
              updateStars();
            }
          });
        }
      };
    });
}());
