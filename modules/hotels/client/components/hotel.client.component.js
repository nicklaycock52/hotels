(function () {
  'use strict';

  var module = angular.module('hotels');

  function controller($scope, HotelsService) {
    var vm = this;

    vm.$onInit = function () {
    };

    vm.isStarRating = function () {
      if (vm.hotel.ratingType === 'star') {
        return true;
      }

      return false;
    };

    vm.getRoomName = function () {
      let roomname = vm.hotel.name !== undefined ? vm.hotel.name : vm.hotel.roomName;
      return roomname;
    };

    vm.hasSavings = function () {
      if (vm.hotel.savings > 0) {
        return true;
      }

      return false;
    };
  }


  module.component('hotel', {
    templateUrl: '/modules/hotels/client/views/hotel.client.component.html',
    controllerAs: 'vm',
    bindings: {
      hotel: '<'
    },
    controller: ['$scope', 'HotelsService', controller]
  });
}());
