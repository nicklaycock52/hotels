(function () {
  'use strict';

  var module = angular.module('hotels');

  function controller($scope, HotelsService) {
    var vm = this;
    vm.hotels = [];
    vm.sortOrder = 1;
    vm.orderBy = '';

    vm.$onInit = function () {
      HotelsService.query(results => {
        vm.hotels = results;
      });
    };
  }


  module.component('hotelList', {
    templateUrl: '/modules/hotels/client/views/hotel-list.client.component.html',
    controllerAs: 'vm',
    bindings: {},
    controller: ['$scope', 'HotelsService', controller]
  });
}());
